@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Edit Data Produk</h3>
                </div>
                <div class="card-body">
                    <form action="{{ route('product.update', $product->id) }}" method="post">
                        @csrf
                        @method('PUT')

                        <div class="form-group">
                            <label for="title">Nama Produk</label>
                            <input type="text" name="title" class="form-control" value="{{ $product->title }}"
                                placeholder="Masukkan nama produk">

                            @if ($errors->has('title'))
                            <span class="text-danger">{{ $errors->first('title') }}</span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="description">Deskripsi</label>
                            <textarea name="description" cols="10" rows="10" class="form-control"
                                placeholder="Masukkan deskripsi produk">{{ $product->description }}</textarea>

                            @if ($errors->has('description'))
                            <span class="text-danger">{{ $errors->first('description') }}</span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="price">Harga</label>
                            <input type="number" name="price" class="form-control" value="{{ $product->price }}"
                                placeholder="Masukkan harga produk">

                            @if ($errors->has('price'))
                            <span class="text-danger">{{ $errors->first('price') }}</span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="stock">Stok</label>
                            <input type="number" name="stock" class="form-control" value="{{ $product->stock }}"
                                placeholder="Masukkan stok produk">

                            @if ($errors->has('stock'))
                            <span class="text-danger">{{ $errors->first('stock') }}</span>
                            @endif
                        </div>

                        <div class="form-group float-right">
                            <button class="btn btn-primary btn-sm">Update</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection