@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Tambah Data Pelanggan</h3>
                </div>

                <div class="card-body">
                    <form action="{{ route('customer.store') }}" method="post">
                        @csrf

                        <div class="form-group">
                            <label for="name">Nama Lengkap</label>
                            <input type="text" name="name"
                                class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}"
                                placeholder="Masukkan nama lengkap">

                            <p class="text-danger">{{ $errors->first('name') }}</p>
                        </div>

                        <div class="form-group">
                            <label for="name">Alamat</label>
                            <textarea name="address" cols="5" rows="5"
                                class="form-control {{ $errors->has('address') ? 'is-invalid' : '' }}"
                                placeholder="Masukkan alamat pelanggan"></textarea>

                            <p class="text-danger">{{ $errors->first('address') }}</p>
                        </div>

                        <div class="form-group">
                            <label for="phone">Phone</label>
                            <input type="text" name="phone"
                                class="form-control {{ $errors->has('phone') ? 'is-invalid' : '' }}"
                                placeholder="Masukkan nomor telepon pelanggan">

                            <p class="text-danger">{{ $errors->first('phone') }}</p>
                        </div>

                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="text" name="email"
                                class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}"
                                placeholder="Masukkan email pelanggan">

                            <p class="text-danger">{{ $errors->first('email') }}</p>
                        </div>

                        <div class="form-group float-right">
                            <button class="btn btn-danger btn-sm">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection