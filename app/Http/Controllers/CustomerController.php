<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Customer;

class CustomerController extends Controller
{
    public function index() {
        $customers = Customer::orderBy("created_at", "DESC")->paginate(10);

        return view("customers.index", compact("customers"));
    }

    public function create() {
        return view("customers.create");
    }

    public function store(Request $request) {
        $this->validate($request, [
            'name' => 'required|string',
            'phone' => 'required|max:13',
            'address' => 'required|string',
            'email' => 'required|email|string|unique:customers,email'
        ]);

        Customer::create($request->except("_token"));
        
        return redirect(route("customer.index"))->with(["success" => "Pelanggan berhasil ditambahkan."]);
    }

    public function edit(Request $request, $id) {
        $customer = Customer::findOrFail($id);

        return view("customers.edit", compact("customer"));
    }

    public function update(Request $request, $id) {
        $this->validate($request, [
            'name' => 'required|string',
            'phone' => 'required|max:13',
            'address' => 'required|string',
            'email' => 'required|email|string|exists:customers,email'
        ]);

        $customer = Customer::findOrFail($id);

        $customer->update($request->except("_token"));
        
        return redirect(route("customer.index"))->with(["success" => "Pelanggan berhasil diubah."]);
    }

    public function destroy($id) {
        $customer = Customer::findOrFail($id);
        $customer->delete();

        return redirect(route("customer.index"))->with(["success" => "Pelanggan berhasil dihapus."]);
    }
}