<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use Illuminate\Support\Str;

class ProductController extends Controller
{
    public function index() {
        $products = Product::orderBy("created_at", "DESC")->get();

        return view("products.index", compact("products"));
    }

    public function create() {
        return view("products.create");
    }

    public function store(Request $request) {
        $validation = $this->validate($request, [
            "title" => "required|string|max:100",
            "description" => "required|string",
            "price" => "required|integer",
            "stock" => "required|integer",
        ]);

        Product::create($request->except("_token"));
        
        return redirect(route("product.index"))->with(["success" => "Produk berhasil ditambahkan."]);
    }

    public function edit($id) {
        $product = Product::findOrFail($id);

        return view("products.edit", compact("product"));
    }

    public function update(Request $request, $id) {
        $validation = $this->validate($request, [
            "title" => "required|string|max:100",
            "description" => "required|string",
            "price" => "required|integer",
            "stock" => "required|integer",
        ]);

        $product = Product::findOrFail($id);

        $product->update($request->except("_token"));

        return redirect(route("product.index"))->with(["success" => "Produk berhasil diubah."]);
    }

    public function destroy($id) {
        $product = Product::findOrFail($id);
        $product->delete();

        return redirect(route("product.index"))->with(["success" => "Produk berhasil dihapus."]);
    }
}