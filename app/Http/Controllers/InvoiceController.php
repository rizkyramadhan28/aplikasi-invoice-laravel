<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer;
use App\Invoice;
use App\Product;
use App\Invoice_detail;

class InvoiceController extends Controller
{
    public function create() {
        $customers = Customer::orderBy("created_at", "DESC")->get();

        return view("invoices.create", compact("customers"));
    }

    public function store(Request $request) {
        $this->validate($request , [
            "customer_id" => "required|exists:customers,id"
        ]);

        $invoice = Invoice::create([
            "customer_id" => $request->customer_id,
            "total" => 0
        ]);

        return redirect(route("invoice.edit", $invoice->id))->with(["success" => "Invoice berhasil ditambahkan."]);
    }

    public function edit($id) {
        $invoice = Invoice::with(["customer", "detail", "detail.product"])->find($id);
        $products = Product::orderBy("created_at", "DESC")->get();

        return view("invoices.edit", compact("invoice", "products"));
    }

    public function update(Request $request, $id) {
        $this->validate($request, [
            'product_id' => 'required|exists:products,id',
            'qty' => 'required|integer'
        ]);

        $invoice = Invoice::find($id);
        $product = Product::find($request->product_id);
        $invoice_detail = $invoice->detail()->where('product_id', $product->id)->first();

        if ($invoice_detail) {
            //MAKA DATA TERSEBUT DI UPDATE QTY NYA
            $invoice_detail->update([
                'qty' => $invoice_detail->qty + $request->qty
            ]);
            
        } else {
            //JIKA TIDAK MAKA DITAMBAHKAN RECORD BARU
            Invoice_detail::create([
                'invoice_id' => $invoice->id,
                'product_id' => $request->product_id,
                'price' => $product->price,
                'qty' => $request->qty
            ]);
        }

        return redirect()->back()->with(['success' => 'Product telah ditambahkan']);
    }

    public function destroy($id) {
        $detail = Invoice_detail::find($id);
        //KEMUDIAN DIHAPUS
        $detail->delete();
        //DAN DI-REDIRECT KEMBALI
        return redirect()->back()->with(['success' => 'Product telah dihapus']);
    }
}